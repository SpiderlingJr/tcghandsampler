import React, { Component } from 'react';
//import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from './pages/Home';
import Simulator from './pages/Simulator';
import NavBar from './components/NavBar';
import Footer from './components/Footer';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      simulator: false,
      entireDeck : [],
      currentDeck: [],
      memory: [],
      pokemonCards: [],
      trainerCards: [],
      energyCards: [],
      input: "",
    }
    this.startSimulator = this.startSimulator.bind(this);
    this.setupDeck = this.setupDeck.bind(this);
  }

  setupDeck(decklist) {
    console.log(decklist)
    let comp = this;
    // TODO replace language differences
    // TODO case insensitivity
    var pattern =  /\s*\n\s*/;
    var cardPattern = /\d+\s\w+/;
    var copyCountPattern = /\d/;
    var currentDecklist = decklist;

    //Get pokemonCards
    var pokemonList;
    currentDecklist = currentDecklist.substr(currentDecklist.search("##Pokémon"));
    pokemonList = currentDecklist.substr(currentDecklist.search(/\n\*/), currentDecklist.search("##Trainerkarten"));
    pokemonList = pokemonList.split(pattern);
    pokemonList = pokemonList.filter(item => cardPattern.test(item)); // Filters array entrys of format " * d1 w* w3 d3 "
    //  console.log(pokemonList);

    //Get trainerCards
    var trainerList;
    currentDecklist = currentDecklist.substr(currentDecklist.search("##Trainerkarten"));
    trainerList = currentDecklist.substr(currentDecklist.search(/\n\*/), currentDecklist.search(/\n#/));
    trainerList = trainerList.split(pattern);
    trainerList = trainerList.filter(item => cardPattern.test(item));
    //  console.log(trainerList);

    //Get energyCards
    var energyCards;
    currentDecklist = currentDecklist.substr(currentDecklist.search("##Energie"));
    energyCards = currentDecklist.substr(currentDecklist.search(/\n\*/), currentDecklist.search(/$/));
    energyCards = energyCards.split(pattern);
    energyCards = energyCards.filter(item => cardPattern.test(item));
    //  console.log(energyCards);

    //Magical function to get the first number of the nth-Element in List. Wow.
    //console.log("Copies of 3 item card iiiis:"+trainerList[2].charAt(trainerList[2].indexOf(trainerList[2].match(/\d/))));
    //var deck = new Array();
    var deck = new Array();

    // Add each pokemon card to deck
    pokemonList.forEach(function(entry) {
      var copyCount = entry.charAt(entry.indexOf(entry.match(copyCountPattern)));
      var cardName = entry.substr(entry.indexOf(entry.match(/[a-zA-z]/)));
      for(var i = 0; i < copyCount; i++) {
        console.log(entry);
        deck.push({name: cardName, type: "pokemon"});
      }
    });

    // Add each trainer card to deck
    trainerList.forEach(function(entry) {
      var copyCount = entry.charAt(entry.indexOf(entry.match(copyCountPattern)));
      var cardName = entry.substr(entry.indexOf(entry.match(/[a-zA-z]/)));
      for(var i = 0; i < copyCount; i ++) {
        console.log(entry);
        deck.push({name: cardName, type: "trainer"});
      }
    });

    // Add each energy card to deck
    energyCards.forEach(function(entry) {
      var copyCount = entry.charAt(entry.indexOf(entry.match(copyCountPattern)));
      var cardName = entry.substr(entry.indexOf(entry.match(/[a-zA-z]/)));
      for(var i = 0; i < copyCount; i ++) {
        console.log(entry);
        deck.push({name: cardName, type: "energy"});
      }
    });
    // push deck to entireDeck state
    comp.setState({entireDeck: comp.state.entireDeck.push(deck)});
    console.log(comp.state.entireDeck);
      this.startSimulator();


  }

  startSimulator(){
    this.setState({simulator : !this.state.simulator});
    console.log(this.state.simulator);
  }

  render() {
    return (
        <div className="App">
          <NavBar/>
          {this.state.simulator ? <Simulator /> : <Home
            startSimulator={this.startSimulator}
            setupDeck={this.setupDeck}
            trala={this.state.input}
            />}
                {this.state.input}
          <Footer/>

        </div>
    );
  }
}
