import React, { Component } from 'react';
//import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

export default function Footer() {
    return (
      <div style={styles.footer}>
        <Navbar color="dark" dark>
        <NavItem>DankInfo</NavItem>
        </Navbar>
      </div>
      )
    }

  const styles = {
    footer: {
    position:"fixed",bottom: 0, width:"100%"
    }
  }
