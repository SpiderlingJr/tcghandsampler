import React, { Component } from 'react';
//import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Card,
  CardBody,
  Input,
  Button,
  Collapse,
  Jumbotron,
} from 'reactstrap';

import ShotCarousel from "./ShotCarousel.js";

export default class HomeInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
    }
    this.toggle = this.toggle.bind(this);
  }
  toggle() {
    this.setState({collapse: !this.state.collapse});
  }

  render() {
    return (
      <div style={styles.container}>
        <Jumbotron className={styles.introCard}>
          <h2> Ganz tolle Überschrift! </h2>
          <h6> Ganz tolle Info!! </h6>
          <hr className="my-2" />
          <p> Hier kommt GANZ viel Text rein! Hier kommt GANZ viel Text rein! Hier kommt GANZ viel Text rein! Hier kommt GANZ viel Text rein! Hier kommt GANZ viel Text rein! Hier kommt GANZ viel Text rein! Hier kommt GANZ viel Text rein! Hier kommt GANZ viel Text rein! Hier kommt GANZ viel Text rein! Hier kommt GANZ viel Text rein! Hier kommt GANZ viel Text rein!
          </p>
         <ShotCarousel/>
         <br/>
         <Button onClick={this.toggle} style={{ marginBottom: '1rem'}}>Formathilfe</Button>
         <Collapse isOpen={this.state.collapse}>
           <Card>
             <CardBody>
               Jolladüü
             </CardBody>
           </Card>
         </Collapse>
        </Jumbotron>
      </div>
    );
  }
}

const styles = {
  container: {
    backgroundColor:"#E9ECEF",
    borderRadius:"4px",
    height:"100vh"
  },
  introCard: {
    align: "center",
    marginTop: "2.5%",
  },
};
