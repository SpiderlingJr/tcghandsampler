import React, { Component } from 'react';
//import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

export default function NavBar() {
    return (
      <div className="App">
        <Navbar color="dark" dark expand="md">
          <NavbarBrand href="/">TCG Hand Sampler</NavbarBrand>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink href="/"> Test </NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/sampler">HandSampler</NavLink>
            </NavItem>
          </Nav>
        </Navbar>
        </div>
      )
    }
