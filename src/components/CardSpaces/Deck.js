import React, { Component } from 'react';
//import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
//import './Commonstyles.js';

import {
  Button,
} from 'reactstrap';

export default class extends Component {
  render() {
    return (
      <div>
      <Button className="Button" size="sm" style={{borderRadius: "0 0 6px 0px"}}>
        {this.props.title}
      </Button>
      </div>
      )
    }
  }

  const styles = {
    footer: {
    position:"fixed",bottom: 0, width:"100%"
    }
  }
