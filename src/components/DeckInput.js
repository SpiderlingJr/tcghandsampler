import React, { Component } from 'react';
//import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Card,
  Input,
  Button,
} from 'reactstrap';
//import  text from './decksample.json';

export default class DeckInput extends Component {
constructor(props){
  super(props);
  this.state = {
    input : "",
    errorMsgInput: false,
    errorMsg: "",
    format: "",

  }
  this.checkInput = this.checkInput.bind(this);
  this.checkFormat = this.checkFormat.bind(this);
  this.throwError = this.throwError.bind(this);
}
  handleChange = name => event => {
  this.setState({
    [name]: event.target.value,
  })
//  console.log(this.state.input);
  }
  checkInput() {

    if (this.state.input !== "") {
      console.log("defined.");
      this.checkFormat(this.state.input);

    } else {
    this.throwError("No Input!");
    }
  }

  throwError(message) {
    console.log(message);
      this.setState({errorMsg: message, errorMsgInput: true});
  }
  checkFormat(input) {
    // Sample Deck
    if(input==="bz0") {
      console.log(decksample.text)
     this.props.setupDeck(decksample.text);
    } else
        //TCGO Format
    if (input.includes("##Pokémon") && input.includes("##Trainer")
        && input.includes("##Energ")) {
          console.log("viable input");
          this.setState({format: "tcgo"});
          console.log("format:" + this.state.format);
          this.props.setupDeck(input);
          //Simple Format
        } else if(input.includes("Pokemon") && input.includes("Pokémon") && !input.includes("www.pokemon.de/tcgo")) {
          console.log("bad input");
          this.setState({format: "simple"});
              console.log("format:" + this.state.format);
          //Other, Wrong Format, unreadable.
        } else {
          console.log("Bad format. ");
        this.throwError("Bad format. Please use the TCGO Format or the Simple Format.For help, see Description: \"Format Help\" Please notice that TCGOHandSampler "+
        "currently only takes English and German variants for automatically generated Decklists.");
         }
  }
  render() {
    return (
      <div>
        <br/>
        <h2>Buwack</h2>
        <p> Deckliste hier rein: </p>
        <Card
          style={styles.introCard}
        >
        <Input onChange={this.handleChange('input')} type="textarea" name="text" style={{resize:"none", height:"100%"}}></Input>
        </Card>
        <Button onClick={this.checkInput} block>
        Submit Deck!
        </Button>
        {this.state.errorMsgInput? <p style={{color: "rgb(226, 78, 25)"}}> {this.state.errorMsg} </p> : <div/>}
      </div>
    );
  }
}

const styles = {
  introCard: {
    align: "center",
    height:"60vh",
    marginTop: "2.5%",
  },
};

const decksample = {
  "text" : "****** Pokémon Sammelkartenspiel-Deckliste ******\n##Pokémon - 19"+
"* 1 Ditto {*} LOT 154\n"+
"* 1 Kommandutan SUM 113\n"+
"* 2 Sniebel UPR 73\n"+
"* 2 Snibunna UPR 74\n"+
"* 1 Diancie {*} FLI 74\n"+
"* 4 Masskito FLI 77\n"+
"* 2 Schneckmag CES 23\n"+
"* 2 Magcargo CES 24\n"+
"* 2 Unratütox GRI 50\n"+
"* 2 Deponitox GRI 51"+
"##Trainerkarten - 31"+
"* 1 Rettungstrage GRI 130"+
"* 2 Tausch CES 147"+
"* 4 Nestball SUM 123"+
"* 1 Cynthia UPR 119"+
"* 3 Bromley BUS 115"+
"* 4 Hyperball SLG 68"+
"* 2 Wahlband GRI 121"+
"* 3 Richter LOT 209"+
"* 4 Lilly UPR 125"+
"* 4 Schrein der Bestrafung CES 143"+
"* 1 Kunstrad CES 123"+
"* 2 Prof. Kukui SUM 128"+
"##Energie - 10"+
"* 1 Bestien-Energie {*} FLI 117"+
"* 4 Regenbogen-Energie SUM 137"+
"* 1 Konter-Energie CIN 100"+
"* 4 Aggregat-Energie {F}{D}{Y} FLI 118"+
"Karten insgesamt: - 60"+
"****** Deckliste erstellt von: Pokémon Sammelkartenspiel Online www.pokemon.de/tcgo ******"
}
