import React, { Component } from 'react';
import { Card, CardImg, Collapse ,CardText, CardBody,
  Jumbotron, CardTitle, CardSubtitle, Button, DropdownItem} from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';

//import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Footer from '../components/Footer.js';
import NavBar from '../components/NavBar.js';
import DeckInput from '../components/DeckInput.js';
import HomeInfo from '../components/HomeInfo.js';



export default class Home extends Component {
  render() {
    return (
        <div id="introPage" className="container-fluid">
          <div class="row">
            <div className="col">
            {this.props.trala}
              <DeckInput
                startSimulator={this.props.startSimulator}
                setupDeck={this.props.setupDeck}/>
            </div>
            <div className="col-8">
              <HomeInfo/>
            </div>
          </div>
        </div>
    );
  }
}
