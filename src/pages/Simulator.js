import React, { Component } from 'react';
import { Card, CardImg, Collapse ,CardText, CardBody,
  Jumbotron, CardTitle, CardSubtitle, Button, DropdownItem} from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';


//import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Footer from '../components/Footer.js';
import NavBar from '../components/NavBar.js';
import DeckInput from '../components/DeckInput.js';
import HomeInfo from '../components/HomeInfo.js';
import Active from '../components/CardSpaces/Active.js';
import Prices from '../components/CardSpaces/Prices.js';
import Deck from '../components/CardSpaces/Deck.js';
import Hand from '../components/CardSpaces/Hand.js';
import Discard from '../components/CardSpaces/Discard.js';
import Bank from '../components/CardSpaces/Bank.js';

export default class Simulator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      entireDeck : [],
      currentDeck: [],
      memory: [],
    }
}


  render() {
    return (
        <div style={{}}>
          <Container style={styles.container} fluid>
            <Row>
             <Col style={styles.header}>General-Menu</Col>
            </Row>
            <Container fluid>
              <Row style={styles.topRow}>
                 <div style={styles.prices}>
                  <Prices title={"Prices"}/>
                 </div>
                 <div style={styles.fillerLeft}>
                 </div>
                 <div style={styles.active}>
                 <Active title={"Active"}/>
                 </div>
                 <div style={styles.fillerRight}>
                 </div>
                 <div style={styles.deck}>
                  <Deck title={"Deck"}/>
                 </div>
              </Row>
              <Row style={styles.middleRow}>
                <div style={styles.bank}>
                  <Bank title={"Bank"}/>
                </div>
                <div style={styles.discard}>
                  <Discard title={"Discard"}/>
                </div>
              </Row>
              <Row style={styles.bottomRow}>
                <div style={styles.hand}>
                 <Hand title={"Hand"}/>
                </div>
              </Row>
            </Container>
          </Container>
         </div>
    );
  }
}

const styles = {
  topRow: {
    position:"relative",
    backgroundColor:"rgb(241, 223, 242)",
    height:"37vh",
    border:"solid",
    borderWidth:"thin",
    borderBottom:"none"
  },
  middleRow: {
    height:"27vh",
    border:"solid",
    borderWidth:"thin",
  },
  bottomRow: {
    height:"27vh",
    borderRight:"solid",
    borderLeft:"solid",
    borderWidth:"thin",
    backgroundColor:"rgb(67, 164, 162)"

  },
  fillerRight: {
    backgroundColor:"rgb(241, 223, 242)",
    width:"33.20%",
  },
  fillerLeft: {
    backgroundColor:  "rgb(241, 223, 242)",
    width: "8.35%",
  },
  header: {
    backgroundColor:"rgb(60, 65, 70)",
    color: "white",
    height: "4vh",
  },
  prices: {
    width:"25.05%",
    //borderRight:"solid",
    borderWidth:"thin",
  },
  active: {
    backgroundColor:"rgb(118, 198, 78)",
    border:"solid",
    width:"16.7%",
    height:"27vh",
    borderWidth:"thin",
    borderBottom: "none",
    borderRadius: "6px 6px 0 0",
    position:"absolute",
    bottom:"0",
    left:"33.4%",

  },
  deck: {
    width:"16.7%",
    height:"27vh",
    backgroundColor:"rgb(118, 198, 78)",
    border:"solid",
    borderWidth:"thin",
    borderBottom:"none",
    borderRight:"none",
    position:"absolute",
    bottom:0,
    right:0,
  },
  discard: {
    backgroundColor:"rgb(78, 183, 198)",
    width:"16.7%",
    borderLeft:"thin solid"
  },
  bank: {
    width:"83.3%",
  },
  hand: {

  },
  container: {
    backgroundColor:"inherit"
  }
}
